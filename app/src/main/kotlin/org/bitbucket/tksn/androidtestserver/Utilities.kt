package org.bitbucket.tksn.androidtestserver

import android.util.Base64
import java.io.File
import java.util.*

/**
 * Decodes Base64-encoded bytes and save it as APK file
 *
 * @param encodedString Base64 encoded data
 * @param dir where to save APK file
 */
fun decodeAndSaveAsApk(encodedString: String, dir: File) : File {
    val bytes: ByteArray = Base64.decode(encodedString, Base64.DEFAULT)
    val tempFileName = UUID.randomUUID().toString() + ".apk"
    val file = File(dir, tempFileName)
    file.writeBytes(bytes)
    return file
}
