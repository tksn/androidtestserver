package org.bitbucket.tksn.androidtestserver

import android.content.Context
import com.koushikdutta.async.AsyncServer
import com.koushikdutta.async.http.server.AsyncHttpServer
import com.koushikdutta.async.http.server.AsyncHttpServerRequest
import org.json.JSONObject

/**
 * Test Runner HTTP server
 *
 * @constructor Initializes the server (but doesn't start it)
 * @property port tcp port number used by the HTTP server
 */
class TestServer(context: Context, val port: Int = 5005) {
    private var mDie : Boolean
    private val mContext : Context
    private val mHttpServer : AsyncHttpServer

    init {
        mDie = false
        mContext = context
        mHttpServer = AsyncHttpServer()
    }

    /**
     * Starts the server
     */
    fun startServer() {
        mHttpServer.get("/ping") { request, response -> response.send(ping(request)) }
        mHttpServer.post("/run") { request, response -> response.send(run(request)) }
        mHttpServer.post("/die") { request, response -> response.send(die(request)) }
        mHttpServer.listen(AsyncServer.getDefault(), port)
    }

    /**
     * Stops the server
     */
    fun stopServer() {
        mHttpServer.stop()
    }

    /**
     * Wait until "Die" flag
     */
    fun waitUntilDie() {
        while (!mDie) {
            Thread.sleep(100)
        }
    }

    /**
     * Checks request from a client
     */
    private fun checkRequest(request: AsyncHttpServerRequest, requestJobj: JSONObject) : JSONObject? {
        val bodyContent: Any? = request.body?.get()
        if (bodyContent == null) {
            requestJobj.put("result", "FAILED")
            requestJobj.put("error", "failed to get request body")
            return null
        }
        if (bodyContent !is JSONObject) {
            requestJobj.put("result", "FAILED")
            requestJobj.put("error", "invalid request format (not a JSON)")
            return null
        }
        return bodyContent
    }

    /**
     * Handles ping request from a client
     */
    private fun ping(request: AsyncHttpServerRequest) : JSONObject {
        val jobj = JSONObject()
        val bodyContent = checkRequest(request, jobj)
        if (bodyContent == null)
            return jobj

        jobj.put("result", "SUCCEEDED")
        jobj.put("pong", bodyContent)
        return jobj
    }

    /**
     * Handles run request from a client
     */
    private fun run(request: AsyncHttpServerRequest) : JSONObject {
        val jobj = JSONObject()
        val bodyContent = checkRequest(request, jobj)
        if (bodyContent == null)
            return jobj

        val apkData: String? = bodyContent.optString("apk_data", null)
        if (apkData == null) {
            jobj.put("result", "FAILED")
            jobj.put("error", "invalid request format (no apk data specified)")
            return jobj
        }

        val className: String? = bodyContent.optString("class_name", null)
        if (className == null) {
            jobj.put("result", "FAILED")
            jobj.put("error", "invalid request format (no class name specified)")
            return jobj
        }

        try {
            TestRunnerService.runApk(mContext, apkData, className)
        }
        catch (e: Throwable) {
            jobj.put("result", "FAILED")
            jobj.put("error", "could not start (${e.message})")
            return jobj
        }

        jobj.put("result", "SUCCEEDED")
        return jobj
    }

    /**
     * Handles ping request from a client
     */
    private fun die(request: AsyncHttpServerRequest) : JSONObject {
        val jobj = JSONObject()
        val bodyContent = checkRequest(request, jobj)
        if (bodyContent == null)
            return jobj

        jobj.put("result", "SUCCEEDED")
        mDie = true
        return jobj
    }
}
