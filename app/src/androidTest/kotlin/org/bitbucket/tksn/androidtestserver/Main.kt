package org.bitbucket.tksn.androidtestserver

import android.support.test.InstrumentationRegistry
import org.junit.After
import org.junit.Before
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.runners.MethodSorters


/**
 * Entry point of androidTest
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class Main {

    val PORT = 5005
    val server = TestServer(InstrumentationRegistry.getTargetContext(), PORT)

    @Before
    fun setUp() {
        server.startServer()
    }

    @After
    fun tearDown() {
        server.stopServer()
    }

    @Test
    fun ZZ_runMainloop() {
        server.waitUntilDie()
    }
}
