import base64
import os
import subprocess
import sys
import time
import pytest
import requests


GRADLE_INSTALL_TASKS = ['installDebug', 'installDebugAndroidTest']
GRADLE_OPTIONS = ['--console=plain']
if sys.platform == 'win32':
    GRADLEW = 'gradlew.bat'
else:
    GRADLEW = './gradlew'

TESTPACKAGE = 'org.bitbucket.tksn.androidtestserver.test'
TESTRUNNER = 'android.support.test.runner.AndroidJUnitRunner'

FORWARD_REMOTE_PORT = 5005
FORWARD_LOCAL_PORT = 5995
SERVERURL = 'http://127.0.0.1:{}/'.format(FORWARD_LOCAL_PORT)
ENDPOINT_PING = SERVERURL + 'ping'
ENDPOINT_RUN = SERVERURL + 'run'
ENDPOINT_DIE = SERVERURL + 'die'


def perr(s):
    print(s, file=sys.stderr)


def start_testrunner():
    stop_testrunner()
    proc = subprocess.Popen(
        ['adb', 'shell', 'am', 'instrument', '-r', '-w',
        '/'.join((TESTPACKAGE, TESTRUNNER))])
    return proc


def stop_testrunner(runnerproc=None):
    if runnerproc is not None:
        runnerproc.kill()
        runnerproc.communicate()
    subprocess.run(['adb', 'shell', 'am', 'force-stop', TESTPACKAGE])


def start_forwarding():
    localport = 'tcp:{}'.format(FORWARD_LOCAL_PORT)
    remoteport = 'tcp:{}'.format(FORWARD_REMOTE_PORT)
    proc = subprocess.run(
        ['adb', 'forward', localport, remoteport])
    return proc.returncode


def stop_forwarding():
    localport = 'tcp:{}'.format(FORWARD_LOCAL_PORT)
    proc = subprocess.run(['adb', 'forward', '--remove', localport])


def wait_for_server(wait_until_die=False):
    PING_COUNT = 20
    PING_INTERVAL = 5.0  # sec

    def ping():
        try:
            pong = requests.get(ENDPOINT_PING, json={'dummy': 1234})
        except requests.RequestException:
            return None
        return pong.json()

    for i in range(PING_COUNT):
        pong = ping()
        if pong is not None:
            pongdata = pong.get('pong')
            if not pongdata or pongdata.get('dummy') != 1234:
                raise RuntimeError('Test server response error')
        if wait_until_die and pong is None:
            return
        if not wait_until_die and pong is not None:
            return
        time.sleep(PING_INTERVAL)

    raise RuntimeError('Test server timeout')


@pytest.fixture(scope='session', autouse=True)
def android_testserver(request):
    scriptdir = os.path.dirname(os.path.abspath(__file__))
    projectdir = os.path.normpath(os.path.join(scriptdir, '..', '..'))
    os.chdir(projectdir)
    perr('-----BUILD_START------------')
    gradle_command = [GRADLEW] + GRADLE_INSTALL_TASKS + GRADLE_OPTIONS
    perr('command = {}'.format(gradle_command))
    proc = subprocess.run(gradle_command)
    perr('-----BUILD_FINISH ({})-----'.format(proc.returncode))
    if proc.returncode != 0:
        raise RuntimeError('Build failure')
    perr('--INSTRUMENT_START----------')
    runner_proc = start_testrunner()
    perr('--FORWARDING_START----------')
    if start_forwarding() != 0:
        raise RuntimeError('Could not start tcp forwarding')
    perr('--WAITING FOR TESTSERVER----')
    wait_for_server()
    perr('--TEST_START----------------')
    def fin():
        perr('')
        terminate_testserver()
        perr('--TESTSERVER_STOP----------')
        stop_forwarding()
        perr('--FORWARDING_STOP----------')
        stop_testrunner(runner_proc)
        perr('--INSTRUMENT_STOP----------')
    request.addfinalizer(fin)


TESTDATADIR = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'example_apk')
TESTDATAPACKAGE = 'org.bitbucket.tksn.example'


def read_apk(filename):
    apk_path = os.path.join(TESTDATADIR, filename)
    with open(apk_path, 'rb') as f:
        return base64.b64encode(f.read()).decode('utf-8')


def full(classname):
    return TESTDATAPACKAGE + '.' + classname


def post_run(payload):
    response = requests.post(ENDPOINT_RUN, json=payload)
    return response


def assert_result(expected, response):
    assert expected == response.json().get('result', '')


def terminate_testserver():
    assert_result(
        'SUCCEEDED',
        requests.post(ENDPOINT_DIE, json={'dummy': 'dummy'})
        )
    wait_for_server(wait_until_die=True)


def test_error_against_emptyjson():
    assert_result('FAILED', post_run({}))


def test_error_against_lackingapk():
    assert_result('FAILED', post_run({'class_name': 'abcde'}))


def test_error_against_lackingclassname():
    assert_result('FAILED', post_run({'apk_data': 'abcde'}))


def test_error_against_invalidapkdata():
    assert_result('FAILED', post_run({
        'apk_data': 'invalidapkdatainvalidapkdata',
        'class_name': full('Minimal')
        }))


def test_error_against_invalidclassname():
    assert_result('FAILED', post_run({
        'apk_data': read_apk('minimal_valid.apk'),
        'class_name': 'invalidclassname'
        }))


def test_able_to_run_minimal_script():
    assert_result('SUCCEEDED', post_run({
        'apk_data': read_apk('minimal_valid.apk'),
        'class_name': full('Minimal')
        }))


def test_able_to_run_scriptwithtestlibs():
    assert_result('SUCCEEDED', post_run({
        'apk_data': read_apk('minimal_with_testlibs_valid.apk'),
        'class_name': full('MinimalWithTestLibs')
        }))


def test_unable_to_run_class_without_callable_interface():
    assert_result('FAILED', post_run({
        'apk_data': read_apk('notcallable_invalid.apk'),
        'class_name': full('NotCallable')
        }))


def test_able_to_run_multiclass():
    assert_result('SUCCEEDED', post_run({
        'apk_data': read_apk('multiclass.apk'),
        'class_name': full('Class0')
        }))


def test_able_to_run_classwiththirdpartylib():
    assert_result('SUCCEEDED', post_run({
        'apk_data': read_apk('withthirdpartylib.apk'),
        'class_name': full('WithThirdPartyLib')
        }))
