package org.bitbucket.tksn.example

import java.util.concurrent.Callable

class Class0 : Callable<Int> {
    override fun call() : Int {
        val c1 = Class1()
        c1.call()
        return 0
    }
}
