package org.bitbucket.tksn.example

class NotCallable {
    fun call() : Int {
        return 0
    }
}
