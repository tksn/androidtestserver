package org.bitbucket.tksn.example

import java.util.concurrent.Callable
import android.util.Log
import android.support.test.uiautomator.UiDevice
import android.support.test.InstrumentationRegistry


class LibVersionMismatch : Callable<Int> {
    override fun call() : Int {
        Log.i("LibVersionMismatch", "CALLED!")
        val dev : UiDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())
        dev.pressHome()
        return 1
    }
}
