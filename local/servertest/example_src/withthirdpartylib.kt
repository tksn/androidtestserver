package org.bitbucket.tksn.example

import java.util.concurrent.Callable
import com.orhanobut.logger.Logger
import org.parceler.Parcel


@Parcel
class WithThirdPartyLib : Callable<Int> {
    override fun call() : Int {
        Logger.d("HELLO!")
        return 0
    }
}
