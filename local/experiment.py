import base64
import os
import requests

APKDIR = 'servertest\\example_apk'
PACKAGE = 'org.bitbucket.tksn.example'
CLASSES = [
#    ('minimal_valid.apk', 'Minimal'),
    ('minimal_with_testlibs_valid.apk', 'MinimalWithTestLibs'),
#    ('notcallable_invalid.apk', 'NotCallable'),
#    ('libversionmismatch_invalid.apk', 'LibVersionMismatch'),
#    ('multiclass.apk', 'Class0'),
#    ('withthirdpartylib.apk', 'WithThirdPartyLib'),
]
VALID_APK = 'minimal_valid.apk'
ENDPOINT = 'http://192.168.0.11:5005/run'


def read_apk(filename):
    apk_path = os.path.join(APKDIR, filename)
    with open(apk_path, 'rb') as f:
        return base64.b64encode(f.read()).decode('utf-8')


def create_payload(filename, classname):
    return {
        'apk_data': read_apk(filename),
        'class_name': '.'.join((PACKAGE, classname))
        }


def post_request(endpoint, payload):
    return requests.post(endpoint, json=payload)


def fail_posts():
    yield 0
    print(post_request(ENDPOINT, {}).json())

    yield 1
    print(post_request(ENDPOINT, {'apk_data': 'abcde'}).json())

    yield 2
    print(post_request(ENDPOINT, {'class_name': 'abcde'}).json())

    yield 3
    payload = create_payload(VALID_APK, 'com.abcde.nosuchpackage.Class0')
    print(post_request(ENDPOINT, payload).json())


if __name__ == '__main__':
    #for i in fail_posts():
    #    input(str(i) + ' OK ?')

    for c in CLASSES:
        input('APK:{}, CLASS:{} ?'.format(*c))
        post_request(ENDPOINT, create_payload(c[0], c[1]))
