import argparse
import os
import re
import shutil
import subprocess
import zipfile


class ConfigError(Exception): pass


def get_sourcelist(source_desc, extensions=('.java', '.kt')):
    files = None
    if not os.path.exists(source_desc):
        raise ValueError('{} does not exist'.format(source_desc))
    if os.path.isdir(source_desc):
        files = (os.path.join(source_desc, f) for f in os.listdir(source_desc))
    else:
        files = [source_desc]
    sources = [f for f in files if any(f.endswith(ext) for ext in extensions)]
    if len(set(os.path.splitext(f)[1] for f in sources)) > 1:
        raise ValueError('can not handle multiple language sources')
    return sources


def get_androidsdkhome():
    home = os.environ.get('ANDROID_HOME')
    if not home:
        raise ConfigError('Please set ANDROID_HOME environment variable')
    if not os.path.exists(home):
        raise ConfigError('ANDROID_HOME environment variable is not valid')
    return home


def find_versioned_dir(subdir_relpath, pattern, matchvalue_func, version=''):
    relpath = os.path.join(*subdir_relpath)
    base = os.path.join(get_androidsdkhome(), relpath)
    if not os.path.exists(base):
        raise ConfigError('{} not found in ANDROID_HOME'.format(relpath))

    if version == '':
        ver_regex = re.compile(pattern)
        matchresults = (ver_regex.match(name) for name in os.listdir(base))
        matches = (matchvalue_func(result) for result in matchresults if result)
        maxvalue_match = max(matches, key=lambda e: e[0], default=None)
        if maxvalue_match is None:
            raise ConfigError('there is no available version under {}'.format(base))
        version = maxvalue_match[1]

    versioned_dir = os.path.join(base, version)
    if not os.path.exists(versioned_dir):
        raise ConfigError('{} does not exist'.format(versioned_dir))
    return versioned_dir


def find_buildtool(version=''):
    def matchvalue_func(m):
        return (
            int(m.group(1)) * 10000 + int(m.group(2)) * 100 + int(m.group(3)),
            m.group(0))

    return find_versioned_dir(
        subdir_relpath=['build-tools'],
        pattern=r'(\d+)\.(\d+)\.(\d+)$',
        matchvalue_func=matchvalue_func,
        version=version)


def find_compilesdk(version=-1):
    return find_versioned_dir(
        subdir_relpath=['platforms'],
        pattern=r'android-(\d+)$',
        matchvalue_func=lambda m: (int(m.group(1)), m.group(0)),
        version='' if version < 0 else 'android-{}'.format(version))


def find_testsupport_dir(component, version=''):
    def matchvalue_func(m):
        v2 = 0 if m.group(3) is None else int(m.group(3))
        return (
            int(m.group(1)) * 10000 + int(m.group(2)) * 100 + v2,
            m.group(0))
    return find_versioned_dir(
        subdir_relpath=[
            'extras', 'android', 'm2repository',
            'com', 'android', 'support', 'test'] + component,
        pattern=r'(\d+)\.(\d+)(?:\.(\d+))?$',
        matchvalue_func=matchvalue_func,
        version=version)


def find_testsupport_runner_dir(version=''):
    return find_testsupport_dir(['runner'], version=version)


def find_testsupport_rules_dir(version=''):
    return find_testsupport_dir(['rules'], version=version)


def find_testsupport_uiautomator_dir(version=''):
    ver_part1 = ''
    ver_part2 = ''
    if version:
        m = re.match(r'v(\d+)-([\d\.]+)', version)
        if not m:
            raise ValueError(
                'specified version format is not correct '
                '(it shoudl be like "v18-2.1.2")')
        ver_part1, ver_part2 = 'uiautomator-v{}'.format(m.group(1)), m.group(2)
    testpath = [
        'extras', 'android', 'm2repository',
        'com', 'android', 'support', 'test']
    dir1 = find_versioned_dir(
        subdir_relpath=testpath + ['uiautomator'],
        pattern=r'uiautomator-v(\d+)$',
        matchvalue_func=lambda m: (int(m.group(1)), m.group(0)),
        version=ver_part1)
    return find_testsupport_dir(
        ['uiautomator', os.path.basename(dir1)],
        version=ver_part2)


def find_testsupport_espresso_dir(subcomponent, version=''):
    return find_testsupport_dir(['espresso', subcomponent], version=version)


def extract_jar_from_aar(source_aar, destdir):
    CLASSESJAR = 'classes.jar'
    if not os.path.exists(source_aar):
        raise ValueError('{} not found'.format(source_aar))
    if not os.path.isfile(source_aar):
        raise ValueError('{} is not a file'.format(source_aar))
    source_root, source_ext = os.path.splitext(source_aar)
    if source_ext != '.aar':
        raise ValueError('{} is not an .aar file'.format(source_aar))
    if not os.path.exists(destdir):
        os.mkdir(destdir)
    if not os.path.isdir(destdir):
        raise RuntimeError('can not create libs_cache directory in {}'.format(os.curdir))
    libdirname = os.path.basename(source_root)
    libdirpath = os.path.join(destdir, libdirname)
    classesjarpath = os.path.join(libdirpath, CLASSESJAR)
    if os.path.exists(libdirpath) and os.path.exists(classesjarpath):
        return classesjarpath
    if not os.path.exists(libdirpath):
        os.mkdir(libdirpath)
    with zipfile.ZipFile(source_aar) as zipf:
        if CLASSESJAR not in zipf.namelist():
            raise ValueError('{} does not contain {}'.format(source_aar, CLASSESJAR))
        zipf.extract(CLASSESJAR, path=libdirpath)
    return classesjarpath


def obtain_classes_jars(libdir, destdir):
    if not os.path.isdir(libdir):
        raise ValueError('{} is not a directory'.format(libdir))
    aars = [name for name in os.listdir(libdir) if name.endswith('.aar')]
    if not aars:
        raise ValueError('{} does not contain any aar file'.format(libdir))
    jarpaths = []
    for aar in aars:
        aarpath = os.path.join(libdir, aar)
        jarpaths.append(extract_jar_from_aar(aarpath, destdir))
    return jarpaths


def collect_testsupport_jars(versions, destdir):
    jars = (
        obtain_classes_jars(
            find_testsupport_runner_dir(version=versions.get('test.runner', '')),
            destdir) +
        obtain_classes_jars(
            find_testsupport_rules_dir(version=versions.get('test.rules', '')),
            destdir) +
        obtain_classes_jars(
            find_testsupport_uiautomator_dir(version=versions.get('test.uiautomator', '')),
            destdir) +
        obtain_classes_jars(
            find_testsupport_espresso_dir(
                subcomponent='espresso-core',
                version=versions.get('test.espresso', '')),
            destdir))
    return jars


def find_kotlinc():
    command = 'kotlinc.bat'
    try:
        subprocess.run([command, '-h'], stderr=subprocess.PIPE)
    except FileNotFoundError:
        return None
    return command


def find_javac():
    java_home = os.environ.get('JAVA_HOME')
    if java_home:
        exepath = os.path.join(java_home, 'bin', 'javac.EXE')
        if os.path.exists(exepath):
            return exepath
    command = 'javac'
    try:
        subprocess.run([command, '-version'], stderr=subprocess.PIPE)
    except FileNotFoundError:
        return None
    return command


def find_exec(basedir, filenamelist):
    for filename in filenamelist:
        path = os.path.join(basedir, filename)
        if os.path.exists(path):
            return path
    return None


def configure(versions, destdir, optional_jars=None):
    optional_jars = optional_jars or []

    config = {}

    kotlinc = find_kotlinc()
    if kotlinc:
        config['kotlinc'] = kotlinc

    javac = find_javac()
    if javac:
        config['javac'] = javac

    buildtools_dir = find_buildtool(version=versions.get('build-tools', ''))
    dxpath = find_exec(buildtools_dir, ['dx.bat', 'dx'])
    if not dxpath:
        raise ConfigError('dx not found in {}'.format(buildtools_dir))
    config['dx'] = dxpath

    aaptpath =  find_exec(buildtools_dir, ['aapt.exe', 'aapt'])
    if not aaptpath:
        raise ConfigError('aapt not found in {}'.format(buildtools_dir))
    config['aapt'] = aaptpath

    compilesdk_dir = find_compilesdk(version=versions.get('compilesdk', -1))
    androidjar = os.path.join(compilesdk_dir, 'android.jar')
    if not os.path.exists(androidjar):
        raise ConfigError('android.jar not found in {}'.format(compilesdk_dir))
    testjars = collect_testsupport_jars(versions=versions, destdir=destdir)
    config['jars'] = [androidjar] + testjars + optional_jars

    config['include_jars'] = optional_jars

    return config


def parse_args():
    parser = argparse.ArgumentParser(description='Quick apk builder')
    parser.add_argument(
        'source', type=str,
        help='source file or directory')
    parser.add_argument(
        '-o', '--output', default='', dest='output',
        help='output file name')
    parser.add_argument(
        '-b', '--buildToolVersion', default='', dest='buildtool_ver',
        help='build-tools version used to build')
    parser.add_argument(
        '-s', '--compileSdkVersion', default=-1, dest='sdk_ver',
        help='Android SDK version (-1 means auto-detect)')
    parser.add_argument(
        '-t', '--testSupportLibVersion', default='0.5', dest='testsupport_ver',
        help='runner/rules version')
    parser.add_argument(
        '-u', '--uiautomatorVersion', default='v18-2.1.2', dest='uiautomator_ver',
        help='uiautomator version')
    parser.add_argument(
        '-e', '--espressoVersion', default='2.2.2', dest='espresso_ver',
        help='espresso version')
    parser.add_argument(
        '-l', '--additionalLibs', default='', dest='additional_libs',
        help='Pathes to additional lib(.jar)s separated by semicollon')
    parser.add_argument(
        '-c', '--libs_cache_dir', default='libs_cache', dest='libs_cache_dir',
        help='temporary jar cache dir')
    return parser.parse_args()


BUILDDIR = 'build'
OBJDIR = os.path.join(BUILDDIR, 'obj')
DEXDIR = os.path.join(BUILDDIR, 'dex')


def create_builddir():
    if not os.path.exists(BUILDDIR):
        os.mkdir(BUILDDIR)
    if not os.path.exists(OBJDIR):
        os.mkdir(OBJDIR)
    if not os.path.exists(DEXDIR):
        os.mkdir(DEXDIR)
    return {
        'build': BUILDDIR,
        'obj': OBJDIR,
        'dex': DEXDIR }


def clean_builddir():
    if os.path.exists(BUILDDIR):
        shutil.rmtree(BUILDDIR)


def prepare_build(args):
    sources = get_sourcelist(args.source)
    if not sources:
        raise ValueError('no files to compile')
    lang = 'kt' if sources[0].endswith('.kt') else 'java'

    output = args.output
    if not output:
        output = os.path.basename(os.path.splitext(args.source)[0]) + '.apk'

    versions = {
        'build-tools': args.buildtool_ver,
        'compilesdk': args.sdk_ver,
        'test.runner': args.testsupport_ver,
        'test.rules': args.testsupport_ver,
        'test.uiautomator': args.uiautomator_ver,
        'test.espresso': args.espresso_ver
    }
    optional_jars = args.additional_libs.split(';')

    config = configure(versions, args.libs_cache_dir, optional_jars)
    if lang == 'kt' and not config.get('kotlinc'):
        raise ConfigError('could not find compiler (kotlinc)')
    if lang == 'java' and not config.get('javac'):
        raise ConfigError('could not find compiler (javac)')

    clean_builddir()
    tmpdir = create_builddir()
    return {
        'lang': lang,
        'sources': sources,
        'output': output,
        'tmpdir': tmpdir,
        'config': config }


def kotlin_compile(kotlin_executable, sources, objectdir, libs):
    kotlinc_command = (
        [kotlin_executable] +
        ['-classpath', ';'.join(libs)] +
        ['-d', objectdir] +
        sources)
    subprocess.run(kotlinc_command)


def run_dx(dx_executable, objectdir, include_jars, dexdir):
    if not os.path.exists(dexdir):
        os.mkdir(dexdir)
    options = ['--dex']
    output = os.path.join(dexdir, 'classes.dex')
    dx_command = (
        [dx_executable, '--dex'] +
        ['--output=' + output] +
        [objectdir] + include_jars)
    subprocess.run(dx_command)


def run_aapt(aapt_executable, dexdir, target):
    options = ['package', '-f']
    aapt_command = (
        [aapt_executable, 'package', '-f'] +
        ['-F', target] +
        [dexdir])
    subprocess.run(aapt_command)


def build(lang, sources, output, tmpdir, config):
    if lang == 'kt':
        kotlin_compile(config['kotlinc'], sources, tmpdir['obj'], config['jars'])
    else:
        raise RuntimeError('Not implemented yet!')

    run_dx(config['dx'], tmpdir['obj'], config['include_jars'], tmpdir['dex'])

    run_aapt(config['aapt'], tmpdir['dex'], output)


def main():
    args = parse_args()
    context = prepare_build(args)
    build(**context)


if __name__ == '__main__':
    main()
