import os
import shutil
from unittest.mock import Mock, patch
import pytest
from quickbuild import *


def dummysdk(f):
    dummysdkhome = os.path.abspath(r'testdata\dummysdk')
    def test_func():
        with patch.dict(os.environ, {'ANDROID_HOME': dummysdkhome}):
            f()
    return test_func


def test_get_sourcelist_existing_file():
    assert get_sourcelist(r'testdata\src\a.kt') == [r'testdata\src\a.kt']


def test_get_sourcelist_existing_dir():
    assert get_sourcelist(r'testdata\src') == [
        r'testdata\src\a.kt',
        r'testdata\src\b.kt',
        r'testdata\src\c.kt']


def test_get_sourcelist_nonexisting_dir():
    with pytest.raises(ValueError):
        get_sourcelist(r'testdata\nosuchdir')


def test_get_androidsdkhome():
    dummysdk = os.path.abspath(r'testdata\dummysdk')
    with patch.dict(os.environ, {'ANDROID_HOME': dummysdk}):
        assert dummysdk == get_androidsdkhome()


def test_get_androidsdkhome_notfound():
    dummysdk = os.path.abspath(r'testdata\dummysdk')
    with patch.dict(os.environ, {'ANDROID_HOME': ''}):
        with pytest.raises(ConfigError):
            get_androidsdkhome()
    with patch.dict(os.environ, {'ANDROID_HOME': 'nosuchdir'}):
        with pytest.raises(ConfigError):
            get_androidsdkhome()


@dummysdk
def test_find_buildtool_specificver_found():
    path = find_buildtool(version='24.0.0')
    assert path.endswith('24.0.0')
    assert os.path.exists(path)
    path = find_buildtool(version='23.0.1')
    assert path.endswith('23.0.1')
    assert os.path.exists(path)


@dummysdk
def test_find_buildtool_specificver_notfound():
    with pytest.raises(ConfigError):
        find_buildtool(version='11.1.1')


@dummysdk
def test_find_buildtool_autover_found():
    path = find_buildtool()
    assert path.endswith('24.0.0')
    assert os.path.exists(path)


def test_find_buildtool_autover_notfound():
    dummysdk = os.path.abspath(r'testdata')
    with patch.dict(os.environ, {'ANDROID_HOME': 'testdata'}):
        with pytest.raises(ConfigError):
            find_buildtool()


@dummysdk
def test_find_compilesdk_specificver_found():
    path = find_compilesdk(version=23)
    assert path.endswith('android-23')
    assert os.path.exists(path)
    path = find_compilesdk(version=22)
    assert path.endswith('android-22')
    assert os.path.exists(path)


@dummysdk
def test_find_compilesdk_specificver_notfound():
    with pytest.raises(ConfigError):
        find_compilesdk(version=1)


@dummysdk
def test_find_compilesdk_autover_found():
    path = find_compilesdk()
    assert path.endswith('android-23')
    assert os.path.exists(path)


def test_find_compilesdk_autover_notfound():
    dummysdk = os.path.abspath(r'testdata')
    with patch.dict(os.environ, {'ANDROID_HOME': 'testdata'}):
        with pytest.raises(ConfigError):
            find_compilesdk()


@dummysdk
def test_find_testsupport_runner_dir_specificver_found():
    path = find_testsupport_runner_dir(version='0.5')
    assert path.endswith('0.5')
    assert os.path.exists(path)
    path = find_testsupport_runner_dir(version='0.4')
    assert path.endswith('0.4')
    assert os.path.exists(path)


@dummysdk
def test_find_testsupport_runner_dir_specificver_notfound():
    with pytest.raises(ConfigError):
        find_testsupport_runner_dir(version='0.1')


@dummysdk
def test_find_testsupport_runner_dir_autover_found():
    path = find_testsupport_runner_dir()
    assert path.endswith('0.5')
    assert os.path.exists(path)


def test_find_testsupport_runner_dir_autover_notfound():
    dummysdk = os.path.abspath(r'testdata')
    with patch.dict(os.environ, {'ANDROID_HOME': 'testdata'}):
        with pytest.raises(ConfigError):
            find_testsupport_runner_dir()


@dummysdk
def test_find_testsupport_uiautomator_dir_specificver_found():
    path = find_testsupport_uiautomator_dir(version='v18-2.1.2')
    assert path.endswith('2.1.2')
    assert os.path.exists(path)
    path = find_testsupport_uiautomator_dir(version='v18-2.1.1')
    assert path.endswith('2.1.1')
    assert os.path.exists(path)


@dummysdk
def test_find_testsupport_uiautomator_dir_specificver_notfound():
    with pytest.raises(ConfigError):
        find_testsupport_uiautomator_dir(version='v18-1.2.3')
    with pytest.raises(ConfigError):
        find_testsupport_uiautomator_dir(version='v02-2.1.2')


@dummysdk
def test_find_testsupport_uiautomator_dir_autover_found():
    path = find_testsupport_uiautomator_dir()
    p0, p1 = os.path.split(path)
    assert p0.endswith('v18')
    assert p1.endswith('2.1.2')
    assert os.path.exists(path)


def test_find_testsupport_uiautomator_dir_autover_notfound():
    dummysdk = os.path.abspath(r'testdata')
    with patch.dict(os.environ, {'ANDROID_HOME': 'testdata'}):
        with pytest.raises(ConfigError):
            find_testsupport_uiautomator_dir()


LIBS_CACHE_PATH = 'libs_cache'


def clean_libs_cache(f):
    def testfunc():
        if os.path.exists(LIBS_CACHE_PATH):
            shutil.rmtree(LIBS_CACHE_PATH)
        f()
        if os.path.exists(LIBS_CACHE_PATH):
            shutil.rmtree(LIBS_CACHE_PATH)
    return testfunc


@clean_libs_cache
def test_extract_jar_from_aar_cleanstate():
    aar = os.path.join('testdata', 'uiautomator-v18-2.1.2.aar')
    jarpath = extract_jar_from_aar(aar, LIBS_CACHE_PATH)
    assert jarpath == LIBS_CACHE_PATH + '\\uiautomator-v18-2.1.2\\classes.jar'
    assert os.path.exists(jarpath)


@clean_libs_cache
def test_extract_jar_from_aar_fileexists():
    aar = os.path.join('testdata', 'uiautomator-v18-2.1.2.aar')
    extract_jar_from_aar(aar, LIBS_CACHE_PATH)
    jarpath = extract_jar_from_aar(aar, LIBS_CACHE_PATH)
    assert jarpath == LIBS_CACHE_PATH + '\\uiautomator-v18-2.1.2\\classes.jar'
    assert os.path.exists(jarpath)


@clean_libs_cache
def test_extract_jar_from_aar_direxists():
    aar = os.path.join('testdata', 'uiautomator-v18-2.1.2.aar')
    destpath = 'libs_cache'
    extract_jar_from_aar(aar, LIBS_CACHE_PATH)
    shutil.rmtree(LIBS_CACHE_PATH + '\\uiautomator-v18-2.1.2')
    jarpath = extract_jar_from_aar(aar, LIBS_CACHE_PATH)
    assert jarpath == LIBS_CACHE_PATH + '\\uiautomator-v18-2.1.2\\classes.jar'
    assert os.path.exists(jarpath)


@clean_libs_cache
def test_obtain_classes_jars():
    jars = obtain_classes_jars('testdata', LIBS_CACHE_PATH)
    assert len(jars) == 2
    jar0 = LIBS_CACHE_PATH + '\\uiautomator-v18-2.1.2\\classes.jar'
    jar1 = LIBS_CACHE_PATH + '\\espresso-core-2.2.2\\classes.jar'
    assert jar0 in jars
    assert jar1 in jars


@clean_libs_cache
def test_collect_testsupport_jars():
    jars = collect_testsupport_jars({}, LIBS_CACHE_PATH)
    assert len(jars) == 4
